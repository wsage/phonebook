using System;
using System.Collections;
namespace PhoneBook
{
	public class SortByYearOfBirth : IComparer
	{
		// implement Compare
		int IComparer.Compare (object x, object y)
		{
			Person p1 = (Person)x;
			Person p2 = (Person)y;
			if (p1.yearOfBirth > p2.yearOfBirth)
				return 1;
			if (p1.yearOfBirth < p2.yearOfBirth)
				return -1;
			return 0;
		}
		
		public SortByYearOfBirth ()
		{
		}
	}
}

