using System;
using System.Collections;
using System.IO;
using System.Linq;

namespace PhoneBook
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// The best thing would be to use a Trie data structure;
			// however, let's begin with something simple
			
			ArrayList persons = new ArrayList ();
			
			Person p;
			
			// load records from file here
			if (File.Exists ("records.txt")) {
				using (StreamReader fileIn = new StreamReader("records.txt")) {
					string line;
					int numberOfAttributes = 4;
					string[] temp = new string[numberOfAttributes];
					
					// this is a cycle to process the file
					while ((line=fileIn.ReadLine ()) != null) {
						temp = line.Split (' ');
						if (temp.Length != numberOfAttributes) {
							throw new Exception ("Error in the file.");
						} else {
							p.lastName = temp [0];
							p.firstName = temp [1];
							p.yearOfBirth = int.Parse (temp [2]);
							p.phoneNumber = temp [3];
							persons.Add (p);
						}
					}
				}
			}
			
			for (;;) {
				Console.Clear ();
				Console.WriteLine ("Welcome");
				Console.WriteLine ("Press 1 to add an entry to the phone book");
				Console.WriteLine ("Press 2 to remove an entry from the phone book");
				Console.WriteLine ("Press 3 to search by first name");
				Console.WriteLine ("Press 4 to search by last name");
				Console.WriteLine ("Press 5 to search by phone number");
				Console.WriteLine ("Press 6 to sort records by last name");
				Console.WriteLine ("Press 7 to sort records by year of birth");
				Console.WriteLine ("Press 8 to exit");
				
				switch (Console.ReadLine ()) {
				case "1": // Check if user entered 1 to add a person to the phone book
					Console.Clear ();
					
					Console.WriteLine ("Enter the last name:");
					p.lastName = Console.ReadLine ();
					
					Console.WriteLine ("Enter the first name:");
					p.firstName = Console.ReadLine ();
					
					Console.WriteLine ("Enter the year of birth:");
					int yearOfBirth;
					bool canParseYear = int.TryParse (Console.ReadLine (), out yearOfBirth);
					if (canParseYear == false) {
						Console.WriteLine ("This is not a number!");
						Console.WriteLine ("Press Enter to continue");
						Console.ReadLine ();
						break;
					}
					if (yearOfBirth < 1900) {
						// RIP p.firstName
						Console.WriteLine ("The probability is high that this person is not alive");
						Console.WriteLine ("Press Enter to continue");
						Console.ReadLine ();
						break;
					}
					if (yearOfBirth > DateTime.Now.Year) {
						Console.WriteLine ("Sorry, no underages");
						Console.WriteLine ("Press Enter to continue");
						Console.ReadLine ();
						break;
					}
					p.yearOfBirth = yearOfBirth;
					
					Console.WriteLine ("Enter the phone number:");
					p.phoneNumber = Console.ReadLine ();
					
					persons.Add (p);
					break;
				case "2": // List all entries and let user delete one of them
					Console.Clear ();
					for (int i = 0; i < persons.Count; i++)
						Console.WriteLine ((i + 1).ToString () + " " +
							((Person)persons [i]).lastName + " " +
							((Person)persons [i]).firstName + " " +
							((Person)persons [i]).yearOfBirth + " " +
							((Person)persons [i]).phoneNumber
						);
					Console.WriteLine ("Enter the number of the person you want to delete:");
					int numberToRemove;
					bool canParseNumber = int.TryParse (Console.ReadLine (), out numberToRemove);
					if (canParseNumber == false) {
						Console.WriteLine ("Failed to parse the number");
						Console.WriteLine ("Press Enter to continue");
						Console.ReadLine ();
						break;
					}
					if (numberToRemove < 1 || numberToRemove > persons.Count) {
						Console.WriteLine ("The number is out of range");
						Console.WriteLine ("Press Enter to continue");
						Console.ReadLine ();
						break;
					}
					persons.RemoveAt (numberToRemove - 1);
					
					// print the list of the persons
					Console.Clear ();
					for (int i = 0; i < persons.Count; i++)
						Console.WriteLine ((i + 1).ToString () + " " + 
							((Person)persons [i]).lastName + " " +
							((Person)persons [i]).firstName + " " +
							((Person)persons [i]).yearOfBirth + " " +
							((Person)persons [i]).phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "3": // Search by first name
					Console.Clear ();
					Console.WriteLine ("Enter first name:");
					string firstName = Console.ReadLine ();
					var queryByFirstName = from Person person in persons
						where person.firstName == firstName
							select person;
					
					foreach (Person person in queryByFirstName)
						Console.WriteLine (person.lastName + " " +
							person.firstName + " " +
							person.yearOfBirth + " " +
							person.phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "4": // Search by last name
					Console.Clear ();
					Console.WriteLine ("Enter last name:");
					string lastName = Console.ReadLine ();
					var queryByLastName = from Person person in persons
						where person.lastName == lastName
							select person;
					
					foreach (Person person in queryByLastName)
						Console.WriteLine (person.lastName + " " +
							person.firstName + " " +
							person.yearOfBirth + " " +
							person.phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "5": // Search by phone number
					Console.Clear ();
					Console.WriteLine ("Enter phone number:");
					string phoneNumber = Console.ReadLine ();
					var queryByPhoneNumber = from Person person in persons
						where person.phoneNumber == phoneNumber
							select person;
					
					foreach (Person person in queryByPhoneNumber)
						Console.WriteLine (person.lastName + " " +
							person.firstName + " " +
							person.yearOfBirth + " " +
							person.phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "6": // Sort records by last name
					Console.Clear ();
					persons.Sort (new SortByLastName ());
					
					foreach (Person person in persons)
						Console.WriteLine (person.lastName + " " +
							person.firstName + " " +
							person.yearOfBirth + " " +
							person.phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "7": // Sort records by year of birth
					Console.Clear ();
					persons.Sort (new SortByYearOfBirth ());
					
					foreach (Person person in persons)
						Console.WriteLine (person.lastName + " " +
							person.firstName + " " +
							person.yearOfBirth + " " +
							person.phoneNumber
						);
					
					Console.WriteLine ("Press Enter to continue...");
					Console.ReadLine ();
					break;
				case "8": // Return and save records to the file
					using (StreamWriter fileOut = new StreamWriter("records.txt")) {
						foreach (Person person in persons)
							fileOut.WriteLine (person.lastName + " " +
								person.firstName + " " +
								person.yearOfBirth + " " +
								person.phoneNumber
							);
					}
					return;
				}
			}
					
		}
	}
}
	