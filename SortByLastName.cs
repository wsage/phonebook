using System;
using System.Collections;
namespace PhoneBook
{
	public class SortByLastName : IComparer
	{
		// implement Compare
		int IComparer.Compare (object x, object y)
		{
			Person p1 = (Person)x;
			Person p2 = (Person)y;
			return string.Compare (p1.lastName, p2.lastName);
		}
		
		public SortByLastName ()
		{
		}
	}
}

