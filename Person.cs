using System;
namespace PhoneBook
{
	// we need a lightweight object here,
	// so we use a structure instead of a class
	public struct Person
	{
		public string firstName;
		public string lastName;
		public int yearOfBirth;
		public string phoneNumber;
	}
}

